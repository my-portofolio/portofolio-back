<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddingImageprojectToproject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->text("project_image");
        });
        Schema::table('projects', function (Blueprint $table) {
            $table->text("project_btncolor");
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->dropColumn('project_image');
        });
        Schema::table('projects', function (Blueprint $table) {
            $table->dropColumn('project_btncolor');
        });
    }
}
