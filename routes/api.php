<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['portofolio'])->group(function () {

    Route::get('/','WebApiController@index');

    Route::get('/domaines','WebApiController@domaines');

    Route::get('/experiences','WebApiController@experiences');

    Route::get('/messages','WebApiController@messages');

    Route::get('/projects','WebApiController@projects');

    Route::get('/skills','WebApiController@skills');

    Route::get('/sociales','WebApiController@sociales');

    Route::get('/user','WebApiController@user');
    Route::get('/user_login','WebApiController@userLogin');

    Route::get('/website','WebApiController@website');
});
Route::prefix('portofolio_admin')->middleware('portofolio_admin')->group(function () {
    Route::get('/', function () {
        return 'good';
    });
    Route::get('/domaines', 'AdminController@addingDomaine');
    Route::get('/skills', 'AdminController@addingSkills');
    Route::get('/experiences', 'AdminController@addingExperiences');
    Route::post('/projects', 'AdminController@addingProjects');
    Route::post('/user_desc', 'AdminController@modifyDesc');
    Route::delete('/domaines', 'AdminController@deleteDomaine');
});