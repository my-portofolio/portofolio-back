<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Website extends Model
{
    //
    protected $fillable = [
        'web_name', 'web_title', 'web_brand', 'web_color','web_id'
    ];
    protected $primaryKey = 'web_id';
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
