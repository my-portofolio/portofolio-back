<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{

    protected $fillable = [

        'message_id',
        'message_name',
        'message_subject',
        'message_text'
    ];
    protected $primaryKey = ['message_id'];
    //
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
