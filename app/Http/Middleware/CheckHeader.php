<?php

namespace App\Http\Middleware;

use App\Website;
use Closure;
use Illuminate\Http\Request;

class CheckHeader
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $web_token = Website::all('web_token')->first()['web_token'];
        if ($request->input('Authorization') != $web_token){
            return response()->json(array("response" => "invalid Authorization"));
        }else{
            return $next($request);
        }
    }
}
