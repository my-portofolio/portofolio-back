<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class UserToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userToken = User::all('remember_token')->first()['remember_token'];
        if ($request->input('userToken') != $userToken){
            return response()->json(array("response" => "invalid Authorization"));
        }else{
            return $next($request);
        }
    }
}
