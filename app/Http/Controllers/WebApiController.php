<?php

namespace App\Http\Controllers;

use App\Domaines;
use App\Experiences;
use App\Messages;
use App\Projects;
use App\Skills;
use App\Sociales;
use App\User;
use App\Website;
use Illuminate\Http\Request;

class WebApiController extends Controller
{
    //
    public function index(){
        return response()->json();
    }
    public function domaines(){
        return response()->json(Domaines::all());
    }
    public function experiences(){
        return response()->json(Experiences::all());
    }
    public function messages(){
        return response()->json(Messages::all());
    }
    public function projects(){
        return response()->json(Projects::all());
    }
    public function skills(){
        return response()->json(Skills::all());
    }
    public function sociales(){
        return response()->json(Sociales::all());
    }
    public function user(){
        return response()->json(User::all()->first());
    }
    public function userLogin(Request $request){
        $all = $request->all();
        $user_m =$all['email'];
        $password = $all['password'];
        $user = User::all()
            ->where('user_email', $user_m)
            ->where('user_password', $password);
       if ($user->count() >=1){
           return response()->json($user->first());
       }else{
           return response()->json('null');
       }

    }

    public function website(){
        return response()->json(Website::all()->first());
    }

}
