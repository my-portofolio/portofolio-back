<?php

namespace App\Http\Controllers;


use App\Domaines;
use App\Experiences;
use App\Projects;
use App\Skills;
use App\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    //
    public function addingDomaine(Request $request){
        $domaine = new Domaines();
        $data = json_decode($request->all()['domaine']);
        $domaine->domaine_title = $data->domaine_title;
        $domaine->domaine_icon = $data->domaine_icon;
        $domaine->domaine_description = $data->domaine_description;
        $domaine->domaine_color = $data->domaine_color;
        $domaine->domaine_textcolor = $data->domaine_textcolor;
        $try = $domaine->save();

        if ($try){
            return response()->json(['response'=>'success']);
        }else{
            return response()->json(['response'=>'error']);
        }

    }
    public function addingSkills(Request $request){
        $skills = new Skills();
        $data = json_decode($request->all()['skills']);

        $skills->skills_name = $data->skills_name;
        $skills->skills_pourc = $data->skills_pourc;
        $skills->skills_color = $data->skills_color;
        $try = $skills->save();

        if ($try){
            return response()->json(['response'=>'success']);
        }else{
            return response()->json(['response'=>'error']);
        }

    }
    public function addingExperiences(Request $request){
        $experience = new Experiences();
        $data = json_decode($request->all()['experiences']);
        $experience->exp_title = $data->exp_title;
        $experience->exp_date = $data->exp_date;
        $experience->exp_description = $data->exp_description;
        $try = $experience->save();
        if ($try){
            return response()->json(['response'=>'success']);
        }else{
            return response()->json(['response'=>'error']);
        }

    }
    public function modifyDesc(Request $request){
        $description = $request->all()['description'];
        $user = User::all();
        $user = $user->first();
        $user->user_desc = $description;
        if ($user->save()){
            return response()->json(['response'=>'success']);
        }else{
            return response()->json(['response'=>'error']);
        }

    }
    public function addingProjects(Request $request){
        if ($request->hasFile('project_image')){
            $project = new Projects();
            $project->project_title = $request->project_title;
            $project->project_link = $request->project_link;
            $project->project_color = $request->project_color;
            $project->project_description = $request->project_description;
            $project->project_categorie = $request->project_categorie;
            $project->project_btncolor = $request->project_btncolor;
            $file = $request->project_image;
            $path = $request->project_image->store('public/images');
            $project->project_image = $path;
            $project->save();
            return response()->json(['response' => 'success', 'message'=> 'the project as been saved succesfully']);
        }else{
            return response()->json(['response' =>'error', 'message' => 'no file detected for the project image']);
        }
    }
    public function deleteDomaine(Request $request) {
        $domaine_id = json_decode($request->all()['domaine_id']);
        $domaine = Domaines::find($domaine_id);
        if (is_null($domaine->delete())){
            return response()->json(['response' => 'erreur']);
        }else {
            return response()->json(['response' => 'success']);
        }

    }
}
