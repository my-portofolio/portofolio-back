<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skills extends Model
{
    protected $fillable = [
        'skills_id',
        'skills_name',
        'skills_pourc',
        'skills_color'
    ];
    protected $primaryKey = 'skills_id';
    //
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
