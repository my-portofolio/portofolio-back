<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Domaines extends Model
{

    //
    protected $fillable = [

        'domaine_id',
        'domaine_title',
        'domaine_icon',
        'domaine_description',
        'domaine_color'
    ];

    protected $primaryKey = 'domaine_id';
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
