<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sociales extends Model
{

    protected $fillable = [

        'sociale_id',
        'sociale_title',
        'sociale_icon',
        'sociale_link'
    ];
    protected $primaryKey = ['sociale_id'];
    //
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
