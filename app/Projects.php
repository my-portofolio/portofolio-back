<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projects extends Model
{
    //

    protected $fillable = [
        'project_id',
        'project_title',
        'project_link',
        'project_color',
        'project_categorie',
        'project_description',
    ];
    protected $primaryKey = 'project_id';
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
