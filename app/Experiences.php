<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Experiences extends Model
{
    protected $fillable = [
        'exp_id',
        'exp_title',
        'exp_date',
        'exp_description'
    ];
    protected $primaryKey = 'exp_id';
    //
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
