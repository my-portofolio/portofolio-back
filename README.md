# Project Title

Portofolio back end

## Getting Started

this is the creation of a portofolio for developer or designer totally modular and dynamic
 it is split in two parts the frontend part which is developed thanks to angular 7 as well as the backend part developed thanks to Laravel
### Prerequisites

you need composer

### Installing

clone the project through :
```
    git@gitlab.com:my-portofolio/portofolio-front.git
```
then got to the directorie of the and type

```
composer install
```

And then

## Running

```
php artisan serve
```

## Built With

* [Laravel](https://laravel.com/) - The web framework used


## Authors

* **axel mwenze** - *Initial work* - [axel mwenze](https://gitlab.com/alexandre2908)

## License

This project is licensed under the MIT License
